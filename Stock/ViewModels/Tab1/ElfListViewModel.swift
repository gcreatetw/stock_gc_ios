//
//  ElfListViewModel.swift
//  SPAPP
//
//  Created by AndrewPeng on 2020/9/30.
//  Copyright © 2020 REDPAY - Laurence. All rights reserved.
//

import Foundation
import UIKit
import PKHUD

protocol ElfListViewModelDelegate: NSObject {
    func onFetchCompleted(fetchResult: [Elf])
    func onFetchFailed()
}

class ElfListViewModel: NSObject {
    
    weak var delegate: ElfListViewModelDelegate?
    weak var parentController: UIViewController?
    var isFetching = false
    var elfList = [Elf]()
    
    func getElfList() {

        let semaphore = DispatchSemaphore(value: 1)
        guard isFetching == false else { return }
        isFetching = true

        /// - REMARK:
        /// 串列處理異步 DispatchQueue 的方法
        DispatchQueue.global().async {
            /// REMARK:
            /// 判斷CoreData有無紀錄Elf list
            /// 如果沒有（第一次進入此頁面），則call api download full list
            if CoreDataManager.sharedInstance.retrieve("Elf", predicate: nil, limit: nil) == [] {
                
                semaphore.wait()
                NetWorkManager.sharedInstance.getElfList { (flag, response) in
                    switch flag {
                    case true:
                        for i in response.data {
                            _ = CoreDataManager.sharedInstance.insert("Elf", attributeInfo: i.dict)
                        }
                        semaphore.signal()
                    case false:
                        self.delegate?.onFetchFailed()
                        semaphore.signal()
                    }
                }
                
                semaphore.wait()
                NetWorkManager.sharedInstance.getElfIsGetList { (flag, response) in
                    switch flag {
                    case true:
                        for i in response.data {
                            _ = CoreDataManager.sharedInstance.update("Elf", predicate: "id ==[c] '\(i)'", attributeInfo: ["is_get" : true,
                                                                                                                           "is_haveRead" : true])
                        }
                        /// 撈出更新後的 Elf list
                        // self.elfList = CoreDataManager.sharedInstance.retrieve("Elf", predicate: nil, limit: nil) as? [Elf] ?? [Elf]()
                        // 改寫 closure，確保資料已經全數取得
                        CoreDataManager.sharedInstance.retrieve("Elf", predicate: nil, doneHandler: { (response) in
                            self.delegate?.onFetchCompleted(fetchResult: response as? [Elf] ?? [Elf]())
                        })
                        semaphore.signal()
                    case false:
                        semaphore.signal()
                    }
                }
                
            } else {
                /// 如果有，則撈core data Elf list
                /// read Elf updateTime
                semaphore.wait()
                let fetchResult: [Elf] = CoreDataManager.sharedInstance.retrieve("Elf", predicate: nil, limit: nil) as? [Elf] ?? [Elf]()
                var elfUpdateTime = ""
                for i in fetchResult {
                    elfUpdateTime = i.updated_at ?? ""
                }
                
                let elfUpdateTime2Date = GlobalFunction.getDateFromString(withFormat: "yyyy/MM/dd HH", strDate: elfUpdateTime)
                let currentTime = GlobalFunction.getSysDate()
                                
                guard elfUpdateTime2Date.daysBetweenDate(toDate: currentTime) > 0 else {
                    /// 如果 Elf updateTime 沒變動
                    /// 先call api isget 刷新一次
                    NetWorkManager.sharedInstance.getElfIsGetList { (flag, response) in
                        switch flag {
                        case true:
                            for i in response.data {
                                _ = CoreDataManager.sharedInstance.update("Elf", predicate: "id ==[c] '\(i)'", attributeInfo: ["is_get" : true])
                            }
                            /// 撈出新的 Elf list
                            // 改寫 closure，確保資料已經全數取得
                            CoreDataManager.sharedInstance.retrieve("Elf", predicate: nil, doneHandler: { (response) in
                                self.delegate?.onFetchCompleted(fetchResult: response as? [Elf] ?? [Elf]())
                            })
                            semaphore.signal()
                        case false:
                            semaphore.signal()
                        }
                    }
                    return
                }
                
                semaphore.wait()
                /// 如果 Elf updateTime 大於現在時間一天
                /// call api update list content
                NetWorkManager.sharedInstance.getElfList { (flag, response) in
                    switch flag {
                    case true:
                        /// 不比對內容直接全刪除，再更新最新list
                        _ = CoreDataManager.sharedInstance.delete("Elf", predicate: nil)
                        for i in response.data {
                            _ = CoreDataManager.sharedInstance.insert("Elf", attributeInfo: i.dict)
                        }
                        semaphore.signal()
                    case false:
                        self.delegate?.onFetchFailed()
                        semaphore.signal()
                    }
                }
                
                semaphore.wait()
                /// 更新完最新List後，刷新一次isget api
                NetWorkManager.sharedInstance.getElfIsGetList { (flag, response) in
                    switch flag {
                    case true:
                        for i in response.data {
                            _ = CoreDataManager.sharedInstance.update("Elf", predicate: "id ==[c] '\(i)'", attributeInfo: ["is_get" : true,
                                                                                                                           "is_haveRead" : true])
                        }
                        /// 撈取最新的Elf list
                        // 改寫 closure，確保資料已經全數取得
                        CoreDataManager.sharedInstance.retrieve("Elf", predicate: nil, doneHandler: { (response) in
                            self.delegate?.onFetchCompleted(fetchResult: response as? [Elf] ?? [Elf]())
                        })
                        semaphore.signal()
                    case false:
                        semaphore.signal()
                    }
                }
            }
            self.isFetching = false
        }
    }
}
