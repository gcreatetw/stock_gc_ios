//
//  KeyBoardUtility.swift
//  SPAPP
//
//  Created by LaurenceRedpay20 on 2020/7/28.
//  Copyright © 2020 REDPAY - Laurence. All rights reserved.
//

import Foundation
import UIKit


class KeyboardAccessoryToolbar: UIToolbar {
    
    //REMARK : For the use of toolbar ontop of KeyPad
    
    convenience init() {
        self.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        self.barStyle = .default
        self.isTranslucent = true
        self.tintColor = UIColor(red: 76 / 255, green: 217 / 255, blue: 100 / 255, alpha: 1)

        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.done))
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.cancel))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        self.items = [cancelButton, spaceButton, doneButton]

        self.isUserInteractionEnabled = true
        self.sizeToFit()
    }

    @objc func done() {
        // Tell the current first responder (the current text input) to resign.
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }

    @objc func cancel() {
        // Call "cancel" method on first object in the responder chain that implements it.
        UIApplication.shared.sendAction(#selector(cancel), to: nil, from: nil, for: nil)
    }
}
