//
//  XmasElfListDataModel.swift
//  SPAPP
//
//  Created by LaurenceRedpay20 on 2020/9/27.
//  Copyright © 2020 REDPAY - Laurence. All rights reserved.
//

import Foundation
import SwiftyJSON

struct XmasElfListDataModel {
    var status: String
    var code: String
    var msg: String
    var data: [XmasElfListData]
    
    init(fromJson json: JSON) {
        self.status = json["status"].stringValue
        self.code = json["code"].stringValue
        self.msg = json["msg"].stringValue
        self.data = json["data"].arrayValue.map(){
            XmasElfListData(fromJson: $0)
        }
    }
}

struct XmasElfListData {
    var id: String
    var name: String
    var region: String
    var starsName: String
    var starsNumber: Int
    var narration: String
    var tucao: String
    var updated_at: String
    var isGet: Bool
    var isHaveRead: Bool
    var dict: [String : Any] {
        return [
            "id" : id,
            "name" : name,
            "region" : region,
            "starsName" : starsName,
            "starsNumber" : starsNumber,
            "narration" : narration,
            "tucao" : tucao,
            "updated_at" : updated_at,
            "is_get" : isGet,
            "is_haveRead" : isHaveRead
        ]
    }
    
    init(fromJson json: JSON) {
        self.id = json["id"].stringValue
        self.name = json["name"].stringValue
        self.region = json["region"].stringValue
        self.starsName = json["starsName"].stringValue
        self.starsNumber = json["starsNumber"].intValue
        self.narration = json["narration"].stringValue
        self.tucao = json["tucao"].stringValue
        self.updated_at = GlobalFunction.getSysDateStr()
        self.isGet = false
        self.isHaveRead = false
    }
    
}
