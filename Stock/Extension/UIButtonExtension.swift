//
//  UIButtonExtension.swift
//  SPAPP
//
//  Created by REDPAY - Laurence on 2020/6/15.
//  Copyright © 2020 REDPAY - Laurence. All rights reserved.
//

import Foundation
import UIKit

private var buttonTouchEdgeInsets: UIEdgeInsets?

extension UIButton {
    
    func setShadow(withColor: UIColor,offsetWidth: CGFloat,offsetHeight: CGFloat,opacity: Float ,radius: CGFloat) {
        self.layer.shadowOffset = CGSize(width: offsetWidth, height: offsetHeight)
        self.layer.shadowColor = withColor.cgColor
        self.layer.shadowOpacity = opacity
        self.layer.shadowRadius = radius
    }
    
    convenience init(image: UIImage, tintColor: UIColor? = nil, target: Any? = nil, action: Selector? = nil, backgroundColor:UIColor? = nil, cornerRadius:CGFloat? = 0) {
        
        self.init(type: .system)
        
        if tintColor == nil {
            setImage(image, for: .normal)
        } else {
            setImage(image.withRenderingMode(.alwaysTemplate), for: .normal)
            self.tintColor = tintColor
        }
        
        if let bgColor = backgroundColor {
            self.backgroundColor = bgColor
        }
        
        if let radius = cornerRadius {
            self.layer.cornerRadius = radius
        }
        
        if let action = action {
            addTarget(target, action: action, for: .primaryActionTriggered)
        }
    }
    
    convenience public init(title: String, titleColor: UIColor, font: UIFont = .systemFont(ofSize: 14), backgroundColor: UIColor = .clear, target: Any? = nil, action: Selector? = nil, cornerRadius:CGFloat? = 0, borderColor:UIColor? = nil) {
        
        self.init(type: .system)
        setTitle(title, for: .normal)
        setTitleColor(titleColor, for: .normal)
        self.titleLabel?.font = font
        self.backgroundColor = backgroundColor
        
        if let action = action {
            addTarget(target, action: action, for: .primaryActionTriggered)
        }
        
        if let radius = cornerRadius {
            self.layer.cornerRadius = radius
        }
        
        if let color = borderColor {
            self.layer.borderWidth = 1.5
            self.layer.borderColor = color.cgColor
        }
    }
    
    var touchEdgeInsets:UIEdgeInsets? {
        get {
            return objc_getAssociatedObject(self, &buttonTouchEdgeInsets) as? UIEdgeInsets
        }
        
        set {
            objc_setAssociatedObject(self,
                                     &buttonTouchEdgeInsets,
                                     newValue,
                                     .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    override open func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        var frame = self.bounds
        
        if let touchEdgeInsets = self.touchEdgeInsets {
            frame = frame.inset(by: touchEdgeInsets)
        }
        
        return frame.contains(point);
    }
    
}


