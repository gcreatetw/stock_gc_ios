//
//  WKWebViewExtension.swift
//  SPAPP
//
//  Created by LaurenceRedpay20 on 2020/8/27.
//  Copyright © 2020 REDPAY - Laurence. All rights reserved.
//

import Foundation
import WebKit

extension WKWebView {
    func load(_ urlString: String) {
        if let url = URL(string: urlString) {
            let request = URLRequest(url: url)
            load(request)
        }
    }
}
