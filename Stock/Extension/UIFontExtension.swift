//
//  UIFontExtension.swift
//  SPAPP
//
//  Created by REDPAY - Laurence on 2020/6/9.
//  Copyright © 2020 REDPAY - Laurence. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    
    class func risePingFangTCRegular(ofSize: CGFloat) -> UIFont {
        return UIFont(name: "PingFangTC-Regular", size: ofSize) ?? UIFont.italicSystemFont(ofSize: ofSize)
    }
    
    class func risePingFangTCSemiBold(ofSize: CGFloat) -> UIFont {
        return UIFont(name: "PingFangTC-Semibold", size: ofSize) ?? UIFont.italicSystemFont(ofSize: ofSize)
    }

    class func riseSFPFontRegular(ofSize: CGFloat) -> UIFont? {
        return UIFont(name: "SFProText-Regular", size: ofSize)
    }
    
    class func riseSFPFontBold(ofSize: CGFloat) -> UIFont {
        return UIFont(name: "SFProText-Bold", size: ofSize) ?? UIFont.italicSystemFont(ofSize: ofSize)
    }
    
    class func riseSFPFontSemiBold(ofSize: CGFloat) -> UIFont {
        return UIFont(name: "SFProText-SemiBold", size: ofSize) ?? UIFont.italicSystemFont(ofSize: ofSize)
    }
    
    // 蘋方體
    class func risePFFont(ofSize: CGFloat) ->UIFont {
        return UIFont(name: "PingFangTC-Regular", size: ofSize) ?? .systemFont(ofSize: ofSize)
    }
    
    class func risePFMedium(ofSize: CGFloat) ->UIFont {
        return UIFont(name: "PingFangTC-Medium", size: ofSize) ?? .boldSystemFont(ofSize: ofSize)
    }
    
    class func risePFSemiBold(ofSize: CGFloat) ->UIFont {
        return UIFont(name: "PingFangTC-Semibold", size: ofSize) ?? .boldSystemFont(ofSize: ofSize)
    }
    
    // SF Pro DIsplay
    class func riseSFPDSemiBold(ofSize: CGFloat) ->UIFont {
        return UIFont(name: "SF-Pro-Display-Semibold", size: ofSize) ?? .boldSystemFont(ofSize: ofSize)
    }
}
