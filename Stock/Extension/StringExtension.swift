//
//  StringExtension.swift
//  SPAPP
//
//  Created by Ming Hui Ho on 2020/10/12.
//  Copyright © 2020 REDPAY - Laurence. All rights reserved.
//

import Foundation

extension String {
    
    /// 字串取代
    /// - Parameters:
    ///   - target: 被取代的字串
    ///   - withString: 將替換成的字串
    /// - Returns: 已經變更的字串
    func replace(target: String, withString: String) -> String {
        return self.replacingOccurrences(of: target, with: withString, options: NSString.CompareOptions.literal, range: nil)
    }
    
    
    /// 判斷只有數字的字串
    /// let digitsOnlyYes = "1234567890".containsOnlyDigits
    /// let digitsOnlyNo = "12345+789".containsOnlyDigits
    var containsOnlyDigits: Bool {
        let notDigits = NSCharacterSet.decimalDigits.inverted
        return rangeOfCharacter(from: notDigits, options: String.CompareOptions.literal, range: nil) == nil
    }
    
    /// 只有英數的字串含大小寫
    var isAlphanumeric: Bool {
        !isEmpty && range(of: "[^a-zA-Z0-9]", options: .regularExpression) == nil
    }
    
    /*
     *去掉首尾空格
     */
    var removeHeadAndTailSpace: String {
        let whitespace = NSCharacterSet.whitespaces
        return self.trimmingCharacters(in: whitespace)
    }
    /*
     *去掉首尾空格 包括后面的换行 \n
     */
    var removeHeadAndTailSpacePro: String {
        let whitespace = NSCharacterSet.whitespacesAndNewlines
        return self.trimmingCharacters(in: whitespace)
    }
    /*
     *去掉所有空格
     */
    var removeAllSapce: String {
        return self.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
    }
    
    func validateEmail(candidate: String) -> Bool {
     let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: candidate)
    }
}
