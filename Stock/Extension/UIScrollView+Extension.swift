//
//  UIScrollView+Extension.swift
//  ForContract
//
//  Created by Ming Hui Ho on 2020/7/31.
//  Copyright © 2020 LaurenceRedpay20. All rights reserved.
//

import UIKit

extension UIScrollView {
    func addVerticalContentView(contentView:UIView) {
        addSubview(contentView)
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        contentView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        contentView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
    
    // 只有這樣做才有辦法把直向卷動，這邊不能用 fillSuperview 也不能用 stack
    // https://github.com/andreatoso/UIScrollView-Programmatically
    func addHorizontalContentView(contentView:UIView) {
        addSubview(contentView)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        contentView.heightAnchor.constraint(equalTo: heightAnchor).isActive = true
        contentView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        contentView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
    }
}
