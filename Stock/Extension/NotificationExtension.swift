//
//  NotificationExtension.swift
//  SPAPP
//
//  Created by Ming Hui Ho on 2020/10/6.
//  Copyright © 2020 REDPAY - Laurence. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let riseGetXmasElf               = Notification.Name("rise.getXmasElf")               // 抓到一隻小精靈, userInfo: ["elf": elf] XmasElf
    static let riseGlobalMissionComplete    = Notification.Name("rise.globalMissionComplete")    // 全體任務完成, userInfo: ["mission": mission] XmasMainMission
    static let riseRefreshElfList           = Notification.Name("rise.refreshElfList")
    static let riseFromDeepLink = Notification.Name("rise.riseFromDeepLink")

}
