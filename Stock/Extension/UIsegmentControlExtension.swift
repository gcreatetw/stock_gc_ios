//
//  UIsegmentControlExtension.swift
//  ThunderFire
//
//  Created by laurenceSecuNet on 4/12/20.
//  Copyright © 2020 laurenceSecuNet. All rights reserved.
//

import Foundation
import UIKit

extension UISegmentedControl {
    
    // ===== for ios13.0 tintcolor doesn't work
    func setSelectedSegmentColor(withTextColor color: UIColor, backGroundColor backColor: UIColor) {
        if #available(iOS 13.0, *) {
            if let font = UIFont(name: "PingFangTC-Semibold", size: 14) {
                let titleTextAttributes = [NSAttributedString.Key.foregroundColor: color,
                                           NSAttributedString.Key.font: font]
                self.setTitleTextAttributes(titleTextAttributes, for: .selected)
            } else {
                let titleTextAttributes = [NSAttributedString.Key.foregroundColor: color]
                self.setTitleTextAttributes(titleTextAttributes, for: .selected)
            }
            self.selectedSegmentTintColor = backColor
        } else {
            self.tintColor = UIColor.white;
        }
    }
    
    func setNotSelectedSegmentColor(withTextColor color: UIColor, backGroundColor backColor: UIColor) {
        if #available(iOS 13.0, *) {
            if let font = UIFont(name: "PingFangTC-Semibold", size: 14) {
                let titleTextAttributes = [NSAttributedString.Key.foregroundColor: color,NSAttributedString.Key.font: font]
                self.setTitleTextAttributes(titleTextAttributes, for: .normal)
            } else {
                let titleTextAttributes = [NSAttributedString.Key.foregroundColor: color]
                self.setTitleTextAttributes(titleTextAttributes, for: .normal)
            }

            self.selectedSegmentTintColor = backColor
        } else {
            self.tintColor = UIColor.black;
        }
    }
    // ======= End
    
    // ======= work around for ios 13.0
    func workAroundForIOS13with(selectedForeGround color:UIColor, tint withColor:UIColor, normalTint byColor: UIColor) {
        if #available(iOS 13, *) {
            let tintColorImage = UIImage(color: UIColor.RiseRed)
            // Must set the background image for normal to something (even clear) else the rest won't work
            setBackgroundImage(UIImage(color: backgroundColor ?? .clear), for: .normal, barMetrics: .default)
            setBackgroundImage(tintColorImage, for: .selected, barMetrics: .default)
            setBackgroundImage(UIImage(color: tintColor.withAlphaComponent(0.2)), for: .highlighted, barMetrics: .default)
            setBackgroundImage(tintColorImage, for: [.highlighted, .selected], barMetrics: .default)
            
            setTitleTextAttributes([.foregroundColor: UIColor.RiseRed, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13, weight: .regular)], for: .normal)
            setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13)], for: .selected)
            
            setDividerImage(tintColorImage, forLeftSegmentState: .normal, rightSegmentState: .normal, barMetrics: .default)
            layer.borderWidth = 1
            layer.borderColor = UIColor.RiseRed.cgColor
            
            
        }
    }
    // ======= End
    
    func ensureiOS12Style() {
        if #available(iOS 13, *) {
            let tintColorImage = UIImage(color: UIColor.RiseRed)
            // Must set the background image for normal to something (even clear) else the rest won't work
            setBackgroundImage(UIImage(color: backgroundColor ?? .clear), for: .normal, barMetrics: .default)
            setBackgroundImage(tintColorImage, for: .selected, barMetrics: .default)
            setBackgroundImage(UIImage(color: tintColor.withAlphaComponent(0.2)), for: .highlighted, barMetrics: .default)
            setBackgroundImage(tintColorImage, for: [.highlighted, .selected], barMetrics: .default)
            
            setTitleTextAttributes([.foregroundColor: UIColor.RiseRed, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13, weight: .regular)], for: .normal)
            setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13)], for: .selected)
            
            setDividerImage(tintColorImage, forLeftSegmentState: .normal, rightSegmentState: .normal, barMetrics: .default)
            layer.borderWidth = 1
            layer.borderColor = UIColor.RiseRed.cgColor
        }
    }
    
}
