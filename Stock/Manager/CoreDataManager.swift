//
//  CoreDataManager.swift
//  SPAPP
//
//  Created by AndrewPeng on 2020/9/7.
//  Copyright © 2020 REDPAY - Laurence. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CoreDataManager {

    static let sharedInstance = CoreDataManager()
    let mContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    func insert(_ entityName: String, attributeInfo: [String : Any]) -> Bool {
        let insetData = NSEntityDescription.insertNewObject(forEntityName: entityName, into: mContext)
        
        for (key,value) in attributeInfo {
            insetData.setValue(value, forKey: key)
        }
        
        do {
            try mContext.save()
            return true
        } catch {
            fatalError("insert to coreData error : \(error)")
        }
    }
    
    /// - REMARK: 讀取
    func retrieve(_ entityName: String, predicate: String?, sort:[[String : Bool]]? = nil, limit: Int? = nil) -> [NSManagedObject]? {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        // predicate
        if let mPredicate = predicate {
            request.predicate = NSPredicate(format: mPredicate)
        }
        
        // sort
        if let mSort = sort {
            var sortArr: [NSSortDescriptor] = []
            for sortCond in mSort {
                for (k, v) in sortCond {
                    sortArr.append(
                        NSSortDescriptor(key: k, ascending: v))
                }
            }
            request.sortDescriptors = sortArr
        }
        
        // limit
        if let limitNumber = limit {
            request.fetchLimit = limitNumber
        }
        
        
        do {
            return try mContext.fetch(request) as? [NSManagedObject]
        } catch {
            fatalError("\(error)")
        }
        
    }
    
    /// - REMARK: 讀取
    func retrieve(_ entityName: String, predicate: String?, sort:[[String : Bool]]? = nil, limit: Int? = nil, doneHandler: @escaping([NSManagedObject]?)->()) {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        // predicate
        if let mPredicate = predicate {
            request.predicate = NSPredicate(format: mPredicate)
        }
        
        // sort
        if let mSort = sort {
            var sortArr: [NSSortDescriptor] = []
            for sortCond in mSort {
                for (k, v) in sortCond {
                    sortArr.append(
                        NSSortDescriptor(key: k, ascending: v))
                }
            }
            request.sortDescriptors = sortArr
        }
        
        // limit
        if let limitNumber = limit {
            request.fetchLimit = limitNumber
        }
        
        
        do {
            doneHandler(try mContext.fetch(request) as? [NSManagedObject])
            //return try mContext.fetch(request) as? [NSManagedObject]
        } catch {
            fatalError("\(error)")
        }
        
    }
    
    func update(_ entityName: String, predicate: String?, attributeInfo: [String : Any]) -> Bool {
        if let results = self.retrieve(entityName, predicate: predicate, sort: nil, limit: nil) {
            for result in results {
                for (key, value) in attributeInfo {
                    result.setValue(value, forKey: key)
                }
            }

            do {
                try mContext.save()
                return true
            } catch {
                fatalError("\(error)")
            }
        }
        return false
    }
    
    /// - REMARK: 刪除
    func delete(_ entityName: String, predicate: String?) -> Bool {
        if let results = self.retrieve(entityName, predicate: predicate, sort: nil, limit: nil) {
            for result in results {
                mContext.delete(result)
            }

            do {
                try mContext.save()
                return true
            } catch {
                fatalError("\(error)")
            }
        }
        return false
    }
    
    /// - REMARK: 刪除 entity 資料
    @discardableResult func clearCoreDataStore(entityName: String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext
        let DelAllReqVar = NSBatchDeleteRequest(fetchRequest: NSFetchRequest<NSFetchRequestResult>(entityName: entityName))
        do {
            try managedContext.execute(DelAllReqVar)
        }
        catch {
            print(error)
        }
    }
    
}
