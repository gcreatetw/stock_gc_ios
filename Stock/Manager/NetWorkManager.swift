//
//  NetWorkManager.swift
//  SPAPP
//
//  Created by REDPAY - Laurence on 2020/6/4.
//  Copyright © 2020 REDPAY - Laurence. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
import Combine
import PKHUD

struct ServerApi {
    var urlProductionV1: String {
        return ApiDomain.production.url+"/"+ApiVersion.v1.version
    }
    var urlProductionV2: String {
        return ApiDomain.production.url+"/"+ApiVersion.v2.version
    }
}

enum ApiDomain {
    case uat, sit, mock, production
    var url: String {
        switch self {
        case .uat: return ""
        case .sit: return ""
        case .mock: return "https://acdb7b7f-8afe-4d49-82cb-ea2ae464f511.mock.pstmn.io"
        case .production: return "https://wd8nob9fe6.execute-api.us-east-2.amazonaws.com"
        }
    }
}

enum ApiVersion {
    case v1, v2, v3
    var version: String {
        switch self {
        case .v1: return "v1"
        case .v2: return "v2"
        case .v3: return "v3"
        }
    }
}

enum Header {
    case standard
    case typical
    
    var prefix: HTTPHeaders {
        get {
            switch self {
            case .standard:
                let header : HTTPHeaders = [
                    "riseVersion":"",
                    "device":"iOS",
                    "deviceID":GlobalValue.uuid,
                    "language":"zh-TW",
                    "auth":GlobalValue.token]
                //                        "clientSysVersion": UIDevice.current.systemVersion,
                //                        "screenW": UIScreen.main.bounds.width.description,
                //                        "screenH": UIScreen.main.bounds.height.description,
                return header
            case .typical:
                let header : HTTPHeaders = ["Auth" : DefaultManager.sharedInstance.token]
                return header
            }
        }
    }
}


enum HttpMethodType {
    case post, get
    var inString: String {
        switch self {
        case .get: return "GET"
        case .post: return "POST"
        }
    }
}



final class NetWorkManager {
    
    static let sharedInstance = NetWorkManager()
    
    private let defaultSession = URLSession(configuration: .default)
    private var dataTask: URLSessionTask?
    private let serverApi = ServerApi()
    
    private init () {}
    
    
    //MARK: - Alamofire
    //HTTP request foundation code.
    
    @discardableResult private func baseRequest (
        _ url         : URLConvertible,
        method        : HTTPMethod = .get,
        parameters    : Parameters? = nil,
        encoding      : ParameterEncoding = JSONEncoding.default,
        headers       : HTTPHeaders? = nil,
        isWithSpinner : Bool = true,
        doneHandler   : @escaping (_ responseJSON: JSON?) -> ()) -> DataRequest {
        
        if isWithSpinner {
            //show activity spinner of any kind
            DispatchQueue.main.async {
                HUD.show(.progress)
            }
        }
        
        let config = URLSessionConfiguration.default
        config.timeoutIntervalForRequest = 30
        config.timeoutIntervalForResource = 30
        
        return AF.request(url, method: method, parameters: parameters, encoding: encoding, headers: headers).responseJSON { [unowned self] (AFDataResponse) in
            
            debugPrint(AFDataResponse)
            
            switch AFDataResponse.result {
            case .success(let value):
                
                let jsonValue = JSON(value)
                doneHandler(jsonValue)
                
            case .failure(let err):
                
                _ = JSON(err)
                let message: String
                if let httpCode = AFDataResponse.response?.statusCode {
                    switch httpCode {
                    case 400:
                        message = "\(httpCode):\(err)"
                    case 401:
                        message = "\(httpCode):\(err)"
                    case 403:
                        message = "\(httpCode):\(err)"
                    case 404:
                        message = "\(httpCode):\(err)"
                    case 500:
                        message = "\(httpCode):\(err)"
                    // more to come...
                    default:
                        message = ""
                    }
                }
            }
        }
    }
    
    
    
    /// - REMARK
    /// Splash Page 呼叫
    func postRefresh(wallets: [[String : String]], doneHandler: @escaping (Bool, RefreshDataModel)->()) {
        
        let params = RefreshParams(wallet: wallets)
        
        baseRequest(serverApi.urlProductionV1+"/login/refresh", method: .post, parameters: params.dict, headers: Header.typical.prefix, isWithSpinner: false) { (jsonData) in
            
            let tempJson = RefreshDataModel(fromJson: jsonData!)
            if tempJson.code == "0" {
                doneHandler(true, tempJson)
            } else {
                doneHandler(false, tempJson)
            }
        }
        
    }
    
    /// - REMARK
    /// LogIn Page tap LogIn Btn
    func postLogin(accountName: String, pssd: String, wallets: [[String : String]], doneHandler: @escaping (Bool, LogInDataModel)->()) {
        
        let params = LogInParams(acctName: accountName, pssd: pssd, wallet: wallets)
        
        baseRequest(serverApi.urlProductionV1+"/login", method: .post, parameters: params.dict) { (jsonData) in
            
            DispatchQueue.main.async {
                HUD.hide()
            }
            
            let tempJson = LogInDataModel(fromJson: jsonData!)
            if tempJson.code == "0" {
                doneHandler(true, tempJson)
            } else {
                doneHandler(false, tempJson)
            }
        }
    }
    
    /// - REMARK
    /// LogIn Page tap SignUp Btn
    func postRegister(acctName: String, avtar: String, nickName: String, email: String, pssd: String, doneHandler: @escaping (Bool, RegisterDataModel)-> Void) {
        
        let params = RegisterParams(acctName: acctName, avatar: avtar, nickName: nickName, email: email, pssd: pssd)
        
        baseRequest(serverApi.urlProductionV1+"/register", method: .post, parameters: params.dict, headers: Header.typical.prefix) { (jsonData) in
            
            DispatchQueue.main.async {
                HUD.hide()
            }
            
            let tempJson = RegisterDataModel(fromJson: jsonData!)
            // mock server response 100 equal to success
            if tempJson.code == "0" {
                doneHandler(true, tempJson)
            } else {
                doneHandler(false, tempJson)
            }
        }
    }
    
    /// - REMARK
    /// 申請一級驗證
    func post1faRequest(doneHandler: @escaping (Bool, OnefaRequestDataModel)-> Void) {
        
        baseRequest(serverApi.urlProductionV1+"/register/1fa/request", method: .post, headers: Header.typical.prefix) { (jsonData) in
            
            DispatchQueue.main.async {
                HUD.hide()
            }
            
            let tempJson = OnefaRequestDataModel(fromJson: jsonData!)
            if tempJson.code == "0" {
                doneHandler(true, tempJson)
            } else {
                doneHandler(false, tempJson)
            }
        }
    }
    
    /// - REMARK
    /// 填寫手機號碼, 請server寄送驗證碼
    func post1faSendsms(countryCode: String, mobile: String, doneHandler: @escaping (Bool, OnefaSendsmsDataModel)-> Void) {
        
        let params = OnefaSendSmsParams(verificationId: GlobalValue.verificationId, countryCode: countryCode, mobile: mobile)
        
        baseRequest(serverApi.urlProductionV1+"/register/1fa/sendsms", method: .post, parameters: params.dict, headers: Header.typical.prefix) { (jsonData) in
            
            DispatchQueue.main.async {
                HUD.hide()
            }
            
            let tempJson = OnefaSendsmsDataModel(fromJson: jsonData!)
            if tempJson.code == "0" {
                doneHandler(true, tempJson)
            } else {
                doneHandler(false, tempJson)
            }
        }
    }
    
    /// - REMARK
    /// 確認手機收到的SMS驗證碼
    func post1faVerifyMobile(smsCode: String, doneHandler: @escaping (Bool, OnefaVerifyMobileDataModel)-> Void) {
        
        let params = OnefaVerifyMobileParams(verificationId: GlobalValue.verificationId, verifyCheckId: GlobalValue.verifyCheckId, verifyCode: smsCode)
        
        baseRequest(serverApi.urlProductionV1+"/register/1fa/verifymobile", method: .post, parameters: params.dict, headers: Header.typical.prefix) { (jsonData) in
            
            DispatchQueue.main.async {
                HUD.hide()
            }
            
            let tempJson = OnefaVerifyMobileDataModel(fromJson: jsonData!)
            if tempJson.code == "0" {
                doneHandler(true, tempJson)
            } else {
                doneHandler(false, tempJson)
            }
        }
    }
    
    /// - REMARK
    /// 忘記密碼
    func postSmsRequirement(email: String, doneHandler: @escaping (Bool, ForgotPssdDataModel)-> Void)  {
        
        let params: [String : Any] = ["email" : email]
        
        baseRequest(serverApi.urlProductionV1+"/account/forgotpassword/request", method: .post, parameters: params, headers: Header.typical.prefix) { (jsonData) in
            
            DispatchQueue.main.async {
                HUD.hide()
            }
            
            let tempJson = ForgotPssdDataModel(fromJson: jsonData!)
            if tempJson.code == "0" {
                doneHandler(true, tempJson)
            } else {
                doneHandler(false, tempJson)
            }
        }
    }
    
    /// - REMARK
    /// 驗證申請忘記密碼的驗證碼
    func postSmsAndCheck(forgotVerifyCode: String, doneHandler: @escaping (Bool, CheckSmsDataModel)-> Void) {
        
        let params = CheckSmsParams(forgotVerifyCode: forgotVerifyCode)
        
        baseRequest(serverApi.urlProductionV1+"/account/forgotpassword/check", method: .post, parameters: params.dictionary, headers: Header.typical.prefix) { (jsonData) in
            
            DispatchQueue.main.async {
                HUD.hide()
            }
            
            let tempJson = CheckSmsDataModel(fromJson: jsonData!)
            if tempJson.code == "0" {
                doneHandler(true, tempJson)
            } else {
                doneHandler(false, tempJson)
            }
        }
    }
    
    /// - REMARK
    /// 重設密碼不含舊密碼
    func postRenewPwd(passwordNew: String, doneHandler: @escaping (Bool, RenewPwdDataModel)-> Void) {
        
        let params: [String : Any] = ["passwordNew" : passwordNew.md5Value]
        
        baseRequest(serverApi.urlProductionV1+"/account/forgotpassword/renewpassword", method: .post, parameters: params, headers: Header.typical.prefix) { (jsonData) in
            
            DispatchQueue.main.async {
                HUD.hide()
            }
            
            let tempJson = RenewPwdDataModel(fromJson: jsonData!)
            if tempJson.code == "0" {
                doneHandler(true, tempJson)
            } else {
                doneHandler(false, tempJson)
            }
        }
    }
    
    /// - REMARK
    /// 重設密碼含舊密碼
    func postRenewPwdHasOldPwd(pwdOld: String, pwdNew: String, doneHandler: @escaping (Bool, RenewPwdDataModel)-> Void) {
        
        let params: [String : Any] = ["passwordOld" : pwdOld.md5Value,
                                      "passwordNew" : pwdNew.md5Value]
        
        baseRequest(serverApi.urlProductionV1+"/account/password", method: .put, parameters: params, headers: Header.typical.prefix) { (jsonData) in
            
            DispatchQueue.main.async {
                HUD.hide()
            }
            
            let tempJson = RenewPwdDataModel(fromJson: jsonData!)
            if tempJson.code == "0" {
                doneHandler(true, tempJson)
            } else {
                doneHandler(false, tempJson)
            }
        }
    }
    
    /// - REMARK
    /// 新增錢包
    func postNewWallet(currency: String, doneHandler: @escaping (Bool, CreateNewWalletDataModel)-> Void) {
        
        let params: [String : Any] = ["currency" : currency]
        
        baseRequest(serverApi.urlProductionV2+"/wallet", method: .post, parameters: params, headers: Header.typical.prefix) { (jsonData) in
            
            DispatchQueue.main.async {
                HUD.hide()
            }
            
            let tempJson = CreateNewWalletDataModel(fromJson: jsonData!)
            if tempJson.code == "0" {
                doneHandler(true, tempJson)
            } else {
                doneHandler(false, tempJson)
            }
        }
    }
    
    
    /// - REMARK
    /// Recover wallet
    func postRecoverWallet(walletkey: String, doneHandler: @escaping (Bool, RecoverDataModel)-> Void) {
        
        let params: [String : Any] = ["walletkey" : walletkey,
                                      "currency" : RISECurrencyType.current.rawValue]
        
        baseRequest(serverApi.urlProductionV2+"/recover", method: .post, parameters: params, headers: Header.typical.prefix) { (jsonData) in
            
            DispatchQueue.main.async {
                HUD.hide()
            }
            
            let tempJson = RecoverDataModel(fromJson: jsonData!)
            if tempJson.code == "0" {
                doneHandler(true, tempJson)
            } else {
                doneHandler(false, tempJson)
            }
        }
    }
    
    
    /// - REMARK
    /// 修改個人資料
    func postModifyAcct(avatar: String, nickName: String, email: String, countryCode: String, mobile: String, doneHandler: @escaping (Bool, ModifyAcctDataModel)-> Void) {
        
        let params: [String : Any] = [
            "avatar" : avatar,
            "nickName" : nickName,
            "email" : email,
            "countryCode" : "+886",
            "mobile" : mobile
        ]
        
        baseRequest(serverApi.urlProductionV1+"/account", method: .put, parameters: params, headers: Header.typical.prefix) { (jsonData) in
            
            DispatchQueue.main.async {
                HUD.hide()
            }
            
            let tempJson = ModifyAcctDataModel(fromJson: jsonData!)
            if tempJson.code == "0" {
                doneHandler(true, tempJson)
            } else {
                doneHandler(false, tempJson)
            }
        }
    }
    
    
    /// - REMARK
    /// 使用條款、隱私政策
    func getPrivacy(doneHandler: @escaping (Bool, PrivacyDataModel)-> Void) {
        
        baseRequest(serverApi.urlProductionV1+"/privacy", method: .get, headers: Header.typical.prefix) { (jsonData) in
            
            DispatchQueue.main.async {
                HUD.hide()
            }
            
            let tempJson = PrivacyDataModel(fromJson: jsonData!)
            if tempJson.code == "0" {
                doneHandler(true, tempJson)
            } else {
                doneHandler(false, tempJson)
            }
        }
    }
    
    /// - REMARK
    /// 常見問題
    func getHelp(doneHandler: @escaping (Bool, HelpDataModel)-> Void) {
        
        baseRequest(serverApi.urlProductionV1+"/help", method: .get, headers: Header.typical.prefix) { (jsonData) in
            
            DispatchQueue.main.async {
                HUD.hide()
            }
            
            let tempJson = HelpDataModel(fromJson: jsonData!)
            if tempJson.code == "0" {
                doneHandler(true, tempJson)
            } else {
                doneHandler(false, tempJson)
            }
        }
    }
    
    /// - REMARK
    /// Logout
    func putLogout() {
        
        baseRequest(serverApi.urlProductionV1+"/logout", method: .get, headers: Header.typical.prefix, isWithSpinner: false) { (jsonData) in
            aPrint("Logout...")
        }
        
    }
    
    /// - REMARK
    /// 最新Ｎ筆交易紀錄
    func postTransactionQueryltest(walletkey: String, doneHandler: @escaping (Bool, TransRecordDataModel)-> Void) {
        
        let params: [String : Any] = [
            "walletkey" : walletkey,
            "currency" : RISECurrencyType.current.rawValue,
            "record_num" : "10"
        ]
        
        baseRequest(serverApi.urlProductionV1+"/transaction/querylatest", method: .post, parameters: params, headers: Header.typical.prefix) { (jsonData) in
            
            DispatchQueue.main.async {
                HUD.hide()
            }
            
            let tempJson = TransRecordDataModel(fromJson: jsonData!)
            if tempJson.code == "0" {
                doneHandler(true, tempJson)
            } else {
                doneHandler(false, tempJson)
            }
        }
    }
    
    /// - REMARK
    /// 最新Ｎ筆交易紀錄By月份
    func postTransactionQueryByMonth(walletkey: String, year: String, month: String,  doneHandler: @escaping (Bool, TransRecordDataModel)-> Void) {
        
        let params: [String : Any] = [
            "walletkey" : walletkey,
            "currency" : RISECurrencyType.current.rawValue,
            "year" : year,
            "month" : month
        ]
        
        baseRequest(serverApi.urlProductionV1+"/transaction/querybymonth", method: .post, parameters: params, headers: Header.typical.prefix) { (jsonData) in
            
            DispatchQueue.main.async {
                HUD.hide()
            }
            
            let tempJson = TransRecordDataModel(fromJson: jsonData!)
            if tempJson.code == "0" {
                doneHandler(true, tempJson)
            } else {
                doneHandler(false, tempJson)
            }
        }
    }
    
    //Wallet Transaction (22)
    func postWalletTransaction(walletkey: String, currency: String, amount: Int, addressTo: String ,doneHandler: @escaping(Bool, WalletTransactionDataModel)->() ) {
        
        let params = WalletTransactionParams(currency: currency, walletKey: walletkey, addressTo: addressTo, amount: amount)
        
        baseRequest(serverApi.urlProductionV1+"/transaction", method: .post, parameters: params.dict, headers: Header.typical.prefix) { (jsonData) in
            
            DispatchQueue.main.async {
                HUD.hide()
            }
            
            let tempJson = WalletTransactionDataModel(fromJson: jsonData!)
            if tempJson.code == "0" {
                doneHandler(true, tempJson)
            } else {
                doneHandler(false, tempJson)
            }
            
        }
    }
    
    //Wallet Recover (25)
    func postWalletRecover(walletkey: String, currency: String, doneHandler: @escaping(Bool, WalletRecoverDataModel)->() ) {
        
        let params = [
            "walletkey" : walletkey,
            "currency" : currency
        ]
        
        baseRequest(serverApi.urlProductionV2+"/recover", method: .post, parameters: params, headers: Header.typical.prefix) { (jsonData) in
            
            DispatchQueue.main.async {
                HUD.hide()
            }
            
            let tempJson = WalletRecoverDataModel(fromJson: jsonData!)
            if tempJson.code == "0" {
                doneHandler(true, tempJson)
            } else {
                doneHandler(false, tempJson)
            }
        }
    }
    
    //Wallet queryBalance (28)
    func postWalletqueryBalance(walletkey: String, currency: String, doneHandler: @escaping(Bool, WalletQueryBalanceDataModel)->()) {
        
        let params = [
            "walletkey" : walletkey,
            "currency" : currency
        ]
        
        baseRequest(serverApi.urlProductionV2+"/querybalance", method: .post, parameters: params, headers: Header.typical.prefix, isWithSpinner: false) { (jsonData) in
            
            let tempJson = WalletQueryBalanceDataModel(fromJson: jsonData!)
            if tempJson.code == "0" {
                doneHandler(true, tempJson)
            } else {
                doneHandler(false, tempJson)
            }
        }
        
    }
    
    //進入 profile 前比對一次資料
    func getProfileInfo(updatedAt: String, doneHandler: @escaping(Bool, LogInDataModel)->()) {
        
        let header: HTTPHeaders = [
            "Auth" : DefaultManager.sharedInstance.token,
            "updated_at" : updatedAt
        ]
        
        baseRequest(serverApi.urlProductionV1+"/account?updated_at=", method: .get, headers: header) { (jsonData) in
            
            DispatchQueue.main.async {
                HUD.hide()
            }
            
            let tempJson = LogInDataModel(fromJson: jsonData!)
            if tempJson.code == "0" {
                doneHandler(true, tempJson)
            } else {
                doneHandler(false, tempJson)
            }
        }
    }
    
    //Elf List Check
    func getElfList(doneHandler: @escaping(Bool, XmasElfListDataModel)->()) {
        
        baseRequest("https://29013072-0aea-40fe-b9ed-74f27db15166.mock.pstmn.io/v1/xmas/elf/list", method: .get, isWithSpinner: false) { (jsonData) in
            
            DispatchQueue.main.async {
                HUD.hide()
            }
            
            let tempJson = XmasElfListDataModel(fromJson: jsonData!)
            if tempJson.code == "0" {
                doneHandler(true, tempJson)
            } else {
                doneHandler(false, tempJson)
            }
        }
    }
    
    //Elf is get
    func getElfIsGetList(doneHandler: @escaping(Bool, XmasElfIsGetDataModel)->()) {
        
        baseRequest("https://29013072-0aea-40fe-b9ed-74f27db15166.mock.pstmn.io/v1/xmas/elf/isget", method: .get) { (jsonData) in
            
            DispatchQueue.main.async {
                HUD.hide()
            }
            
            let tempJson = XmasElfIsGetDataModel(fromJson: jsonData!)
    
            if tempJson.code == "0" {
                doneHandler(true, tempJson)
            } else {
                doneHandler(false, tempJson)
            }
        }
    }
    
    //Elf share
    func getElfShareData(elfId: String, doneHandler: @escaping(Bool, XmasElfShareDataModel)->()) {
        
        baseRequest("https://29013072-0aea-40fe-b9ed-74f27db15166.mock.pstmn.io/v1/xmas/elf/share/\(elfId)", method: .get) { (jsonData) in
            
            DispatchQueue.main.async {
                HUD.hide()
            }
            
            let tempJson = XmasElfShareDataModel(fromJson: jsonData!)
    
            if tempJson.code == "0" {
                doneHandler(true, tempJson)
            } else {
                doneHandler(false, tempJson)
            }
        }
    }
    
    
    //Elf download
    func getElfDownload(elfId: String, doneHandler: @escaping(Bool, XmasElfDownloadDataModel)->()) {
        
        baseRequest("https://29013072-0aea-40fe-b9ed-74f27db15166.mock.pstmn.io/v1/xmas/elf/download/\(elfId)", method: .get) { (jsonData) in
            
            DispatchQueue.main.async {
                HUD.hide()
            }
            
            let tempJson = XmasElfDownloadDataModel(fromJson: jsonData!)
    
            if tempJson.code == "0" {
                doneHandler(true, tempJson)
            } else {
                doneHandler(false, tempJson)
            }
        }
    }
    
    
    
    //get 200$
    func get200(addressTo: String, doneHandler: @escaping(Bool, TransactionDataModel)->()) {
        
        let params: [String : Any] = [
            "currency" : RISECurrencyType.current.rawValue,
            "type" : "icWalletLineRise" ,
            "walletkey" : "Mv+BAwEBBldhbGxldAH/ggABAgEKUHJpdmF0ZUtleQH/hAABCVB1YmxpY0tleQEKAAAALv+DAwEBClByaXZhdGVLZXkB/4QAAQIBCVB1YmxpY0tleQH/hgABAUQB/4gAAAAv/4UDAQEJUHVibGljS2V5Af+GAAEDAQVDdXJ2ZQEQAAEBWAH/iAABAVkB/4gAAAAK/4cFAQL/igAAAEf/ggEBARljcnlwdG8vZWxsaXB0aWMucDI1NkN1cnZl/4sDAQEJcDI1NkN1cnZlAf+MAAEBAQtDdXJ2ZVBhcmFtcwH/jgAAAFP/jQMBAQtDdXJ2ZVBhcmFtcwH/jgABBwEBUAH/iAABAU4B/4gAAQFCAf+IAAECR3gB/4gAAQJHeQH/iAABB0JpdFNpemUBBAABBE5hbWUBDAAAAP4Bb/+M/70BASEC/////wAAAAEAAAAAAAAAAAAAAAD///////////////8BIQL/////AAAAAP//////////vOb6racXnoTzucrC/GMlUQEhAlrGNdiqOpPns+u9VXaYhrxlHQawzFOw9jvOPD4n0mBLASECaxfR8uEsQkf4vOblY6RA8ncDfYEt6zOg9KE5RdiYwpYBIQJP40Li/hp/m47n60p8D54WK84zV2sxXs7LtkBoN79R9QH+AgABBVAtMjU2AAABIQJx4ZnZvsOCeJ+FUGu2+Vm3FVtzZZv0HFVzC9O/fqVaZwEhAk1Qb7mv4Xzvn9gXdE1LExXuHJ1wSg78I0HNIa+HJOdWAAEhAuUS+vF0Ee3j3MuI8X5Zh5qdIERAuA6M/5tPKLqgckzJAAFAceGZ2b7DgnifhVBrtvlZtxVbc2Wb9BxVcwvTv36lWmdNUG+5r+F875/YF3RNSxMV7hydcEoO/CNBzSGvhyTnVgA=",
            "address_to" : addressTo,
            "amount" : 200
        ]
        baseRequest(serverApi.urlProductionV1+"/transaction/", method: .post, parameters: params, isWithSpinner: false) { (jsonData) in
            let tempJson = TransactionDataModel(fromJson: jsonData!)
            if tempJson.code == "0" {
                doneHandler(true, tempJson)
            } else {
                doneHandler(false, tempJson)
            }
        }
    }
    
}





